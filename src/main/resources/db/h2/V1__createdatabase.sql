CREATE TABLE "bcpperu".rol (
  id_rol INT AUTO_INCREMENT  PRIMARY KEY,
  nombre_rol VARCHAR(10) NOT NULL,
  codigo_rol VARCHAR(4) NOT NULL,
  fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP not null
);

CREATE TABLE "bcpperu".usuario (
  id_usuario INT AUTO_INCREMENT  PRIMARY KEY,
  nombre_usuario VARCHAR(100) NOT NULL,
  codigo_usuario VARCHAR(4) NOT NULL,
  login_usuario VARCHAR(20) NOT NULL,
  password_usuario VARCHAR(100) NOT NULL,
  fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP not null,
  id_rol int,
  foreign key (id_rol) references rol(id_rol)
);

CREATE TABLE "bcpperu".tipo_cambio (
  id_detalle INT AUTO_INCREMENT  PRIMARY KEY,
  codigo_moneda_origen VARCHAR(4) NOT NULL,
  codigo_moneda_destino VARCHAR(4) NOT NULL,
  costo decimal(10,2) NOT NULL
);
