package com.bcp.exchangemoney.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bcp.exchangemoney.entity.Usuario;

@Repository
@Transactional
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	@Query(nativeQuery = true, value="SELECT * FROM usuario ue " + 
			"WHERE ue.USERNAME=(:usuario) " + 
			"AND ue.PASSWORD = (:password) ")
	Usuario loginUsuario(@Param("usuario")String username, @Param("password")String password);
	
	Usuario findByUser(@Param("username") String username);
}
