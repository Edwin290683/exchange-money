package com.bcp.exchangemoney.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bcp.exchangemoney.entity.Rol;

@Repository
@Transactional
public interface RolRepository extends JpaRepository<Rol, Integer> {
	@Query(nativeQuery = false,value="SELECT r.nombre FROM Rol r INNER JOIN Usuario u ON r.id = u.rol.id WHERE u.id=:idUsuario")
	List<String> obtenerRolesXUsuario(@Param("idUsuario") Integer idUsuario);
}
 