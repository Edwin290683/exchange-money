package com.bcp.exchangemoney.repository;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bcp.exchangemoney.entity.TipoCambio;

@Repository
@Transactional
public interface TipoCambioRepository extends JpaRepository<TipoCambio, Integer> {
	@Query(nativeQuery = false, value="SELECT a FROM TipoCambio a WHERE a.origen =(:origen) and a.destino =(:destino)")
	TipoCambio obtenerTipoCambio(@Param("origen") String origen, @Param("destino") String destino);
	
	@Modifying
	@Query(nativeQuery = false, value="UPDATE TipoCambio tp set tp.costo =(:costo) where tp.destino =(:destino) and tp.origen =(:origen)")
	void actualizarTipoCambio(@Param("origen") String origen, @Param("destino") String destino, @Param("costo") BigDecimal costo);
}
