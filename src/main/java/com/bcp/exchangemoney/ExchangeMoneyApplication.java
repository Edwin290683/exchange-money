package com.bcp.exchangemoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@ComponentScan("com.bcp.exchangemoney*")
@EntityScan("com.bcp.exchangemoney.entity")
@EnableJpaRepositories("com.bcp.exchangemoney.repository")
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class ExchangeMoneyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExchangeMoneyApplication.class, args);
	}

}
