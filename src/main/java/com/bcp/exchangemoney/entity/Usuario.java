package com.bcp.exchangemoney.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Builder
@Table(name="usuario", schema="\"bcpperu\"")
@NamedQuery(name = "Usuario.findAll", query = "SELECT a FROM Usuario a")
public class Usuario {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	private int id;
	
	@Column(name = "nombre_usuario")
	private String nombre;
	
	@Column(name = "codigo_usuario")
	private String codigo;
	
	@Column(name = "login_usuario")
	private String user;
	
	@Column(name = "password_usuario")
	private String password;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion")
	private Date fecha;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_rol")
	private Rol rol;
}
