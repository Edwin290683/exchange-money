package com.bcp.exchangemoney.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Builder
@Table(name="tipo_cambio", schema="\"bcpperu\"")
@NamedQuery(name = "TipoCambio.findAll", query = "SELECT a FROM TipoCambio a")
public class TipoCambio {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_detalle")
	private int id;
	
	@Column(name = "codigo_moneda_origen")
	private String origen;
	
	@Column(name = "codigo_moneda_destino")
	private String destino;
	
	@Column(name = "costo")
	private BigDecimal costo;
}
