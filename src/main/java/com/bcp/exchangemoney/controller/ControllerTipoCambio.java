package com.bcp.exchangemoney.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bcp.exchangemoney.dto.DtoTipoCambio;
import com.bcp.exchangemoney.model.DetalleTipoCambio;

import reactor.core.publisher.Mono;

public interface ControllerTipoCambio {

	public @ResponseBody Mono<ResponseEntity<DtoTipoCambio>> obtenerTipoCambio(@Valid @RequestBody DtoTipoCambio dtoTipoCambio);
	
	public @ResponseBody Mono<ResponseEntity<DetalleTipoCambio>> calcularTipoCambio(@Valid @RequestBody DetalleTipoCambio dtoDetalle);
	
	public @ResponseBody Mono<ResponseEntity<Void>> actualizaTipoCambio(@Valid @RequestBody DtoTipoCambio dtoTipoCambio);

}
