package com.bcp.exchangemoney.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bcp.exchangemoney.dto.DtoUsuario;

public interface ControllerRol {

	public @ResponseBody ResponseEntity<List<String>> obtenerRolesXUsuario(@Valid @RequestBody DtoUsuario dtoUsuario);

}
