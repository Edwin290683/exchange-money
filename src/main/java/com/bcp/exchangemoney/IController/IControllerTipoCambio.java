package com.bcp.exchangemoney.IController;

import java.math.BigDecimal;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.exchangemoney.controller.ControllerTipoCambio;
import com.bcp.exchangemoney.dto.DtoTipoCambio;
import com.bcp.exchangemoney.entity.TipoCambio;
import com.bcp.exchangemoney.model.DetalleTipoCambio;
import com.bcp.exchangemoney.service.ServiceTipoCambio;
import com.github.dozermapper.core.Mapper;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/api/v1/tipocambio")
public class IControllerTipoCambio implements ControllerTipoCambio{
	
	@Autowired
	ServiceTipoCambio serviceTipoCambio;
	
	@Autowired
	private Mapper mapper;
	
	@GetMapping("/get")
	@Override
	public @ResponseBody Mono<ResponseEntity<DtoTipoCambio>> obtenerTipoCambio(@Valid @RequestBody DtoTipoCambio dtoTipoCambio) {
		log.info("obtenemos el tipo de cambio");
	    return serviceTipoCambio.obtenerTipoCambio(mapper.map(dtoTipoCambio, TipoCambio.class)).
				map(tipo -> ResponseEntity.status(HttpStatus.OK).body(mapper.map(tipo, DtoTipoCambio.class))).
				defaultIfEmpty(ResponseEntity.status(HttpStatus.NO_CONTENT).build());
	}

	@PostMapping("/calculo")
	@Override
	public @ResponseBody Mono<ResponseEntity<DetalleTipoCambio>> calcularTipoCambio(@Valid DetalleTipoCambio dtoDetalle) {
		log.info("calculamos el tipo de cambio");
		return serviceTipoCambio.obtenerTipoCambio(mapper.map(dtoDetalle, TipoCambio.class)).
				                map(tipo -> ResponseEntity.status(HttpStatus.OK).body(
				        				DetalleTipoCambio.builder().
				        				costotipocambio(Optional.ofNullable(tipo.getCosto()).orElse(BigDecimal.ZERO)).				        				
				        				montorecibido(Optional.ofNullable(dtoDetalle.getMontorecibido()).orElse(BigDecimal.ZERO)).
				        				montoCambio(dtoDetalle.getMontorecibido().multiply(tipo.getCosto())).
				        				destino(dtoDetalle.getDestino()).
				        				origen(dtoDetalle.getOrigen()).build())).
				                defaultIfEmpty(ResponseEntity.status(HttpStatus.NO_CONTENT).build());

	}
	
	@PostMapping("/actualizar")
	@Override
	public @ResponseBody Mono<ResponseEntity<Void>> actualizaTipoCambio(@Valid @RequestBody DtoTipoCambio dtoTipoCambio) {
		log.info("actualizar el tipo de cambio");
		serviceTipoCambio.actualizar(mapper.map(dtoTipoCambio, TipoCambio.class));
		return Mono.just(ResponseEntity.status(HttpStatus.OK).build());
	}

}
