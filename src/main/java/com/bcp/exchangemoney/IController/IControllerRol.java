package com.bcp.exchangemoney.IController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.exchangemoney.controller.ControllerRol;
import com.bcp.exchangemoney.dto.DtoUsuario;
import com.bcp.exchangemoney.entity.Usuario;
import com.bcp.exchangemoney.service.ServiceRol;
import com.github.dozermapper.core.Mapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/rol")
public class IControllerRol implements ControllerRol{
	
	@Autowired
	ServiceRol serviceRol;
	
	@Autowired
	private Mapper mapper;
	
	@PostMapping("/getForUser")
	@Override
	public @ResponseBody ResponseEntity<List<String>> obtenerRolesXUsuario(@Valid @RequestBody DtoUsuario dtoUsuario) {
		log.info("obtenemos los roles de un ususario "+ dtoUsuario);
		List<String> roles = Optional.ofNullable(serviceRol.obtenerRolesXUsuario(mapper.map(dtoUsuario, Usuario.class))).orElseThrow(NumberFormatException::new);
		List<String> dtoRoles = new ArrayList<>();
		for (String mp : roles) {
			dtoRoles.add(mp);
		}
		return ResponseEntity.status(HttpStatus.OK).body(dtoRoles);
	}

}
