package com.bcp.exchangemoney.IController;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.exchangemoney.IService.IServiceUser;
import com.bcp.exchangemoney.dto.DtoJwt;
import com.bcp.exchangemoney.dto.DtoUsuario;
import com.bcp.exchangemoney.error.IncorrectFileExtensionException;
import com.bcp.exchangemoney.util.JwtTokenUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/auth")
public class IControllerAuthentication {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private IServiceUser iServiceUser;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@PostMapping("/autenticacion")
	public @ResponseBody ResponseEntity<DtoJwt> createAuthenticationToken(@Valid @RequestBody DtoUsuario dtoUsuario) {
		log.info("autenticacion");
		authenticate(dtoUsuario.getUser(), dtoUsuario.getPassword());
		UserDetails userDetails = iServiceUser.loadUserByUsername(dtoUsuario.getUser());
		if(!"".equals(userDetails.getUsername())) {
			String token = jwtTokenUtil.generateToken(userDetails);
			return ResponseEntity.status(HttpStatus.OK).body(new DtoJwt(token));
		}
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	private Authentication authenticate(String username, String password) {
		try {
			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException | BadCredentialsException e) {
			e.printStackTrace();
			throw new IncorrectFileExtensionException("USER_DISABLED", e);
		}
	}
}
