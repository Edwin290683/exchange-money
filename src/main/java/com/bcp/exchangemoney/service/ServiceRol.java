package com.bcp.exchangemoney.service;

import java.util.List;

import com.bcp.exchangemoney.entity.Rol;
import com.bcp.exchangemoney.entity.Usuario;

public interface ServiceRol {

	public List<String> obtenerRolesXUsuario(Usuario usuario);

	public Rol actualizarElemento(Rol rol);

}
