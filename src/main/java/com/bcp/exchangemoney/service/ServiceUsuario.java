package com.bcp.exchangemoney.service;

import java.util.List;

import com.bcp.exchangemoney.entity.Usuario;

public interface ServiceUsuario {

	public List<Usuario> obtenerUsuarios();
	public Usuario obtenerUsuario(Usuario usuario);
	public Usuario actualizarElemento(Usuario usuario);

}
