package com.bcp.exchangemoney.service;

import java.util.List;

import com.bcp.exchangemoney.entity.TipoCambio;

import reactor.core.publisher.Mono;

public interface ServiceTipoCambio {

	public Mono<TipoCambio> obtenerTipoCambio(TipoCambio tipocambio);
	
	public TipoCambio actualizarelemento(TipoCambio tipocambio);

	public List<TipoCambio> obtener();
	
	public Mono<Void> actualizar(TipoCambio tipocambio);

}
