package com.bcp.exchangemoney.IService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.exchangemoney.entity.TipoCambio;
import com.bcp.exchangemoney.repository.TipoCambioRepository;
import com.bcp.exchangemoney.service.ServiceTipoCambio;

import reactor.core.publisher.Mono;

@Service
public class IServiceTipoCambio implements ServiceTipoCambio{
	
	@Autowired
	TipoCambioRepository tipoCambioRepository;

	@Override
	public Mono<TipoCambio> obtenerTipoCambio(TipoCambio tipo) {
		return Mono.just(tipoCambioRepository.obtenerTipoCambio(tipo.getOrigen(), tipo.getDestino()));
	}

	@Override
	public List<TipoCambio> obtener() {
		return tipoCambioRepository.findAll();
	}

	@Override
	public TipoCambio actualizarelemento(TipoCambio tipocambio) {
		return tipoCambioRepository.save(tipocambio);
	}

	@Override
	public Mono<Void> actualizar(TipoCambio tipocambio) {
		tipoCambioRepository.actualizarTipoCambio(tipocambio.getOrigen(), tipocambio.getDestino(), tipocambio.getCosto());
		return Mono.empty();
	}

	

}
