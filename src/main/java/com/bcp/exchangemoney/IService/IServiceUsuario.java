package com.bcp.exchangemoney.IService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.exchangemoney.entity.Usuario;
import com.bcp.exchangemoney.repository.UsuarioRepository;
import com.bcp.exchangemoney.service.ServiceUsuario;

@Service
public class IServiceUsuario implements ServiceUsuario{
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public List<Usuario> obtenerUsuarios() {
		return usuarioRepository.findAll();
	}

	@Override
	public Usuario actualizarElemento(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}

	@Override
	public Usuario obtenerUsuario(Usuario usuario) {
		return usuarioRepository.loginUsuario(usuario.getUser(), usuario.getPassword());
	}

}
