package com.bcp.exchangemoney.IService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bcp.exchangemoney.entity.Rol;
import com.bcp.exchangemoney.entity.Usuario;
import com.bcp.exchangemoney.repository.UsuarioRepository;

@Service
public class IServiceUser implements UserDetailsService{
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario =  usuarioRepository.findByUser(username);
		if(usuario.getCodigo() == null) {
			throw new UsernameNotFoundException("Usuario no encontrado");
		}
		Rol rol = usuario.getRol();
		
		List<GrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority(rol.getNombre()));
		
		return new User(usuario.getUser(), usuario.getPassword(), authorities);
	}

}
