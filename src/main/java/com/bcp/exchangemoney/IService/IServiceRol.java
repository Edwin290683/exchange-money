package com.bcp.exchangemoney.IService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.exchangemoney.entity.Rol;
import com.bcp.exchangemoney.entity.Usuario;
import com.bcp.exchangemoney.repository.RolRepository;
import com.bcp.exchangemoney.service.ServiceRol;

@Service
public class IServiceRol implements ServiceRol{

	@Autowired
	RolRepository rolRepository;
	
	@Override
	public List<String> obtenerRolesXUsuario(Usuario usuario) {
		return rolRepository.obtenerRolesXUsuario(usuario.getId());
	}

	@Override
	public Rol actualizarElemento(Rol rol) {
		return rolRepository.save(rol);
	}

}
