package com.bcp.exchangemoney.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil {
    private String secret = "12345";
    
    private String bearer = "Bearer";
    
    private long jwtTokenValidity = 60;

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("authorities", userDetails.getAuthorities());
        return doGenerateToken(claims, userDetails.getUsername());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        Map<String, Object> header = new HashMap<>();
        header.put(Header.TYPE, "JWT");
        return bearer + " " + 
            Jwts.builder()
                .setHeader(header)
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + jwtTokenValidity*1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public Boolean canTokenBeRefreshed(String token) {
        return (!isTokenExpired(token));
    }

    public boolean validateToken(String token, String user) {
        final String username = getUsernameFromToken(token);
        return (username.equals(user) && !isTokenExpired(token));
    }
    public Collection<GrantedAuthority> setAuthority(List<String> roles) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for(String rol : roles) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + rol));
        }
        return authorities;
    }
    public Collection<GrantedAuthority> getAuthority(String token) {
        final Claims claims = getAllClaimsFromToken(token);
        @SuppressWarnings("unchecked")
        ArrayList<Map<String,String>> roles = (ArrayList<Map<String,String>>)claims.get("authorities");
        
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        roles.forEach(r->
            authorities.add(new SimpleGrantedAuthority(r.get("authority")))
         );

        return authorities;
    }
    public String getToken(String jwt) {
        if (jwt != null && jwt.startsWith("Bearer ")) {
            return jwt.substring(7);
        }
        return null;
    }
}