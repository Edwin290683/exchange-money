package com.bcp.exchangemoney.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DetalleTipoCambio {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal montorecibido;
	private BigDecimal montoCambio;
	private String origen;
	private String destino;
	private BigDecimal costotipocambio;
	
	
	
}
