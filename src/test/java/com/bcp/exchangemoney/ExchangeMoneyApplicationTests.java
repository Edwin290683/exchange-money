package com.bcp.exchangemoney;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.bcp.exchangemoney.entity.Rol;
import com.bcp.exchangemoney.entity.TipoCambio;
import com.bcp.exchangemoney.entity.Usuario;
import com.bcp.exchangemoney.service.ServiceRol;
import com.bcp.exchangemoney.service.ServiceTipoCambio;
import com.bcp.exchangemoney.service.ServiceUsuario;

@SpringBootTest
class ExchangeMoneyApplicationTests {
	
	@Autowired
	ServiceTipoCambio serviceTipoCambio;
	
	@Autowired
	ServiceRol serviceRol;
	
	@Autowired
	ServiceUsuario serviceUsuario;
	
	@Autowired
	BCryptPasswordEncoder bcrypt;

	//@Test
	void agregarTipoCambio() {
		TipoCambio tipo = new TipoCambio(0, "FJD", "PER", BigDecimal.valueOf(2.54));
		
		TipoCambio cambio = serviceTipoCambio.actualizarelemento(tipo);
		
		assertEquals("PER", cambio.getDestino());
		
	}
	
	//@Test
	void agregarUsuario() {
		Rol rol = new Rol(0, "CLIENTE", "R003", new Date());
		serviceRol.actualizarElemento(rol);
		Usuario usuario = new Usuario(0, "Mario", "U003", "mgonzales", bcrypt.encode("12345"), new Date(), rol);
		serviceUsuario.actualizarElemento(usuario);
		assertEquals("CLIENTE", rol.getNombre());
	}


}
