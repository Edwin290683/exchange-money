FROM openjdk:8
ADD build/libs/exchange-money-0.0.1-SNAPSHOT-application.jar exchange-money.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "exchange-money.jar" ]

FROM openjdk:8
ADD h2/h2-1.4.200.jar h2.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "h2.jar" ]